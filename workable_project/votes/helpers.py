__author__ = 'avlahop'
from django.core.cache import cache
from movie_rama.models import Movie


def cache_movies():
    movies = Movie.objects.all().prefetch_related('user', 'votes')
    cache.set('movies', movies)