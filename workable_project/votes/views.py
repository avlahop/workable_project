from django.db import IntegrityError
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.core.cache import cache

# Create your views here.
from movie_rama.models import Movie
from votes.models import Vote
from votes.helpers import cache_movies


def like(request, movie_id):
    '''
    The view adds a like vote to the movie with movie_id for logged in user
    :param request:
    :param movie_id:
    :return: HttpRedirect object
    '''
    user = request.user
    movie = get_object_or_404(Movie, pk=movie_id)

    try:
        vote = Vote.objects.get(user=user, movie=movie)
    except Vote.DoesNotExist:
        vote = None

    if vote:
        vote.is_positive = True
        movie.hate_to_like()
    else:
        vote = Vote(movie=movie, user=user, is_positive=True)
        movie.add_like()
    try:
        vote.save()
        movie.save()
        cache_movies()
    except IntegrityError:
        pass
    finally:
        return redirect("movie_rama-movies")


def hate(request, movie_id):
    '''
    The view adds a hate vote to the movie with movie_id for logged in user
    :param request:
    :param movie_id:
    :return:HttpRedirect object
    '''
    user = request.user
    movie = get_object_or_404(Movie, pk=movie_id)

    try:
        vote = Vote.objects.get(user=user, movie=movie)
    except Vote.DoesNotExist:
        vote = None

    if vote:
        vote.is_positive = False
        movie.like_to_hate()
    else:
        vote = Vote(movie=movie, user=user, is_positive=False)
        movie.add_hate()

    try:
        vote.save()
        movie.save()
        cache_movies()
    except IntegrityError:
        pass
    finally:
        return redirect("movie_rama-movies")


def retract_vote(request, movie_id):
    '''
    The view retracts a vote to the movie with movie_id for logged in user. If the vote was positive it decrements the
    likes counter. If it's is not it decrements the hates counter
    :param request:
    :param movie_id:
    :return:HttpRedirect object
    '''
    user = request.user
    movie = get_object_or_404(Movie, pk=movie_id)

    try:
        vote = Vote.objects.get(user=user, movie=movie)
    except Vote.DoesNotExist:
        vote = None

    if vote:
        if vote.is_positive:
            movie.reduce_counters(vote.is_positive)
        else:
            movie.reduce_counters(vote.is_positive)

        movie.save()
        cache_movies()
        vote.delete()
    return redirect("movie_rama-movies")
