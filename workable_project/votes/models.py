from __future__ import unicode_literals
from django.contrib.auth.models import User

from django.db import models


from movie_rama.models import Movie


class Vote(models.Model):
    movie = models.ForeignKey(Movie, related_name='votes')
    user = models.ForeignKey(User, related_name='votes')
    is_positive = models.BooleanField()

    class Meta:
        verbose_name = "Vote"
        verbose_name_plural = "Votes"
        unique_together = ('movie', 'user')

    def __unicode__(self):
        return "%s voted for %s" % (self.user.get_full_name(), self.movie)
