__author__ = 'avlahop'

from django.conf.urls import *
from votes.views import like, hate, retract_vote

urlpatterns = [
    url(r'^like/(?P<movie_id>\d+)/$', like, name='votes-like'),
    url(r'^hate/(?P<movie_id>\d+)/$', hate, name='votes-hate'),
    url(r'^retract/(?P<movie_id>\d+)/$', retract_vote, name='votes-retract_vote'),

]