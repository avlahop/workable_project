from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
from django.test import TestCase
from django.contrib.auth.models import User

# Create your tests here.
from movie_rama.models import Movie
from votes.models import Vote


class VoteTest(TestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(username="testuser", password="testuser123abc",
                                              first_name="Test", last_name="User")
        self.user1.save()

    def tearDown(self):
        users = User.objects.all()
        movies = Movie.objects.all()

        if users:
            users.delete()

        if movies:
            movies.delete()

        if cache.get('movies'):
            cache.delete('movies')

    def test_user_cannot_add_a_vote_for_the_same_movie(self):
        movie = self.create_movie()
        vote = self.create_positive_vote(movie)
        movie.likes += 1
        movie.save()

        vote = Vote(user=self.user1, movie=movie, is_positive=False)

        with transaction.atomic():
            with self.assertRaises(IntegrityError):
                vote.save()

    def test_adding_a_like_vote_increments_likes(self):
        movie = self.create_movie()

        self.client.login(username=self.user1.username, password="testuser123abc")
        self.client.get(reverse('votes-like', kwargs={"movie_id": movie.pk}), follow=True)

        movie = Movie.objects.get(pk=movie.pk)
        self.assertEqual(1, movie.likes)

    def test_adding_a_hate_increments_hates(self):
        movie = self.create_movie()

        self.client.login(username=self.user1.username, password="testuser123abc")
        self.client.get(reverse('votes-hate', kwargs={"movie_id": movie.pk}), follow=True)

        movie = Movie.objects.get(pk=movie.pk)
        self.assertEqual(1, movie.hates)

    def test_changing_a_vote_from_like_to_hate_changes_movie_counters(self):
        movie = self.create_movie()
        vote = self.create_positive_vote(movie)
        movie.add_like()

        # change vote
        self.client.login(username=self.user1.username, password="testuser123abc")
        self.client.get(reverse('votes-hate', kwargs={"movie_id": movie.pk}), follow=True)

        movie = Movie.objects.get(pk=movie.pk)
        self.assertEqual(0, movie.likes)
        self.assertEqual(1, movie.hates)

    def test_changing_a_vote_from_hate_to_like_changes_movie_counters(self):
        movie = self.create_movie()
        vote = self.create_negative_vote(movie)
        movie.add_hate()

        # change vote
        self.client.login(username=self.user1.username, password="testuser123abc")
        self.client.get(reverse('votes-like', kwargs={"movie_id": movie.pk}), follow=True)

        movie = Movie.objects.get(pk=movie.pk)
        self.assertEqual(0, movie.hates)
        self.assertEqual(1, movie.likes)

    def test_retracting_a_vote_deletes_vote(self):
        movie = self.create_movie()
        vote = self.create_positive_vote(movie)
        movie.likes += 1
        movie.save()

        self.client.login(username=self.user1.username, password="testuser123abc")
        self.client.get(reverse('votes-retract_vote', kwargs={"movie_id": movie.pk}), follow=True)

        with self.assertRaises(Vote.DoesNotExist):
            vote = Vote.objects.get(pk=vote.pk)

    def test_retracting_a_positive_vote_decrements_likes(self):
        movie = self.create_movie()
        vote = self.create_positive_vote(movie)
        movie.likes += 1
        movie.save()

        self.client.login(username=self.user1.username, password="testuser123abc")
        self.client.get(reverse('votes-retract_vote', kwargs={"movie_id": movie.pk}), follow=True)

        movie = Movie.objects.get(pk=movie.pk)
        self.assertEqual(0, movie.likes)

    def test_retracting_a_negative_vote_decrements_hates(self):
        movie = self.create_movie()
        vote = self.create_negative_vote(movie)
        movie.hates += 1
        movie.save()

        self.client.login(username=self.user1.username, password="testuser123abc")
        self.client.get(reverse('votes-retract_vote', kwargs={"movie_id": movie.pk}), follow=True)

        movie = Movie.objects.get(pk=movie.pk)
        self.assertEqual(0, movie.hates)

    def create_movie(self):
        movie = Movie(title="BatmanVsSuperman", description="Martha!!", user=self.user1)
        movie.save()
        return movie

    def create_positive_vote(self, movie):
        vote = Vote(is_positive=True, user=self.user1, movie=movie)
        vote.save()
        return vote

    def create_negative_vote(self, movie):
        vote = Vote(is_positive=False, user=self.user1, movie=movie)
        vote.save()
        return vote
