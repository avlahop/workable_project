__author__ = 'avlahop'

from django.conf.urls import url
from movie_rama.views import *


urlpatterns = [
    url(r'^$', all_movies, name='movie_rama-movies'),
    url(r'^add/$', add, name='movie_rama-add')
]

