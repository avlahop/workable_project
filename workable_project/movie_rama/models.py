from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class Movie(models.Model):
    title = models.CharField(max_length=250)
    description = models.TextField()
    pub_date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)
    likes = models.PositiveIntegerField(default=0)
    hates = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = "Movie"
        verbose_name_plural = "Movies"

    def __unicode__(self):
        return self.title

    def add_like(self):
        self.likes += 1

    def add_hate(self):
        self.hates += 1

    def like_to_hate(self):
        self.likes -= 1
        self.hates += 1

    def hate_to_like(self):
        self.likes += 1
        self.hates -= 1

    def reduce_counters(self, is_positive):
        if is_positive:
            self.likes -= 1
        else:
            self.hates -= 1
