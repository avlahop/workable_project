from django.forms.models import ModelForm
from movie_rama.models import Movie
from django.forms import Textarea, TextInput

__author__ = 'avlahop'


class MovieForm(ModelForm):
    class Meta:
        model = Movie
        fields = ('title', 'description',)

        widgets = {
            'title': TextInput(attrs={'class':"form-control"}),
            'description': Textarea(attrs={'class':'form-control'})
        }
