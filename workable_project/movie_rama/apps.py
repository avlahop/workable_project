from __future__ import unicode_literals

from django.apps import AppConfig


class MovieRamaConfig(AppConfig):
    name = 'movie_rama'
