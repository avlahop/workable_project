__author__ = 'avlahop'
from votes.models import Vote


def get_vote_or_none(request, votes, movie):
    '''
    This function takes the request object and the movie and returns the user's vote for the movie or None
    :param request:
    :param movie:
    :return: User's Vote or None
    '''
    vote = None
    if not request.user.is_anonymous():
        for vote in votes:
            if vote.user_id == request.user.id and vote.movie_id == movie:
                return vote

    return vote


def set_up_response_movies(request, movies, votes, list_of_movies):
    '''
    Helper function that setups up the response movie_items to show in html template
    :param request:
    :param movies:
    :param list_of_movies:
    :return:
    '''

    for movie in movies:
        movie_item = dict(movie=movie)
        vote = get_vote_or_none(request, votes, movie)
        movie_item.update(vote=vote)
        list_of_movies.append(movie_item)
