from django.contrib.auth.models import User
from django.test import TestCase
from django.core.cache import cache

# Create your tests here.
from movie_rama.models import Movie


class MovieRamaTest(TestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(username="testuser", password="testuser123abc",
                                              first_name="Test", last_name="User")

    def tearDown(self):
        users = User.objects.all()
        movies = Movie.objects.all()

        if users:
            users.delete()

        if movies:
            movies.delete()

        if cache.get('movies'):
            cache.delete('movies')

    def test_all_movies_view_returns_zero_results_with_empty_db(self):
        print Movie.objects.all().count()
        self.client.login(username=self.user1.username, password="testuser123abc")
        response = self.client.get('/')
        self.assertEqual(0, len(response.context['movies']))

    def test_all_movies_view_returns_all_movies(self):
        movie = Movie(title="BatmanVsSuperman", description="Martha!!!", user=self.user1)
        movie.save()
        print Movie.objects.all().count()
        number_of_movies = Movie.objects.all().count()

        self.client.login(username=self.user1.username, password="testuser123abc")
        response = self.client.get("/")
        self.assertEqual(number_of_movies, len(response.context['movies']))

    def test_add_movie_redirects_when_user_not_logged_in(self):
        response = self.client.get("/add/", follow=True)
        self.assertRedirects(response, '/accounts/login/?next=/add/')

    def test_add_movie_creates_a_new_movie(self):
        self.client.login(username=self.user1.username, password="testuser123abc")
        data = dict(title="BatmanVsSuperman", description="Martha!!!", user=self.user1.pk)
        response = self.client.post('/add/', data=data, follow=True)
        self.assertEqual(1, Movie.objects.all().count())

    def test_cache_updates_when_adding_a_new_movie(self):
        self.client.login(username=self.user1.username, password="testuser123abc")
        num_of_movies_in_cache = 0
        if cache.get('movies'):
            num_of_movies_in_cache = cache.get('movies').count()

        data = dict(title="BatmanVsSuperman", description="Martha!!!", user=self.user1.pk)
        self.client.post('/add/', data=data, follow=True)

        self.assertEqual(cache.get('movies').count(), num_of_movies_in_cache + 1)
