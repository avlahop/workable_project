from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.contrib import messages
from django.core.cache import cache
from django.contrib.auth.models import User
from movie_rama.models import Movie
from movie_rama.forms import MovieForm
from movie_rama.helpers import set_up_response_movies
from votes.models import Vote


def all_movies(request):
    '''
    The view function to handle the index page to show all the movies.
    :param request:
    :return:
    '''
    movies = cache.get('movies')

    if not movies:
        movies = Movie.objects.all().select_related('user')  # get all movies and prefetch all related models for performance.
        cache.set('movies', movies)

    user_id = request.GET.get('user', None)
    order_field = request.GET.get('sort', None)

    if user_id:
        user = get_object_or_404(User, pk=int(user_id))
        movies = movies.filter(user_id=user.id)  # filter results by user

    if order_field:
        movies = movies.order_by(
            "-" + order_field)  # order them (descending) byt the field specified in the GET parameters

    list_of_movies = []
    votes = Vote.objects.all().select_related('user', 'movie')

    set_up_response_movies(request, movies, votes, list_of_movies)  # create the response arguments for the template

    args = dict(movies=list_of_movies, user_id=user_id, order_field=order_field)

    return render_to_response("movie_rama/movies.html", args, context_instance=RequestContext(request))


@login_required(login_url='/accounts/login/')
def add(request):
    '''
    This view handles the adding of a new movie. If the user is not logged in it redirects him back to the login page
    :param request:
    :return:
    '''
    form = MovieForm(request.POST or None)

    if request.method == "POST" and form.is_valid():
        movie = form.save(commit=False)
        movie.user = request.user
        movie.save()

        cache.set('movies', Movie.objects.all().prefetch_related('user', 'votes'))
        messages.success(request, "Successfully added movie")

        return redirect('movie_rama-movies')

    args = dict(movie_add_form=form)

    return render_to_response("movie_rama/add.html", args, context_instance=RequestContext(request))
