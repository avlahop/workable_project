from django.contrib import messages
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from accounts.forms import RegisterUserForm, LoginUserForm
from django.contrib.auth import login as auth_login, logout as auth_logout, authenticate


# Create your views here.


def perform_login(request, user):
    '''
    This function handles user login process for every view

    :param request: The request object
    :param user: The user we need to login after authentication
    :return: HttpRedirect object
    '''
    print user
    if user:
        if user.is_active:
            auth_login(request, user)
            messages.success(request, "You logged in successfully")
            return redirect('movie_rama-movies')
    else:
        messages.error(request, "Could not login user. Wrong username or password")
        return redirect('accounts-login')


def register(request):
    '''
    The view that handles registration of new users
    :param request:
    :return: HttpRedicect to the specified url
    '''
    register_form = RegisterUserForm(request.POST or None)

    if request.method == "POST" and register_form.is_valid():
        user = register_form.save(commit=True)
        user = authenticate(username=register_form.cleaned_data.get('username'),
                            password=register_form.cleaned_data.get('password1'))

        return perform_login(request, user)

    args = dict(register_form=register_form)

    return render_to_response('accounts/register.html', args, context_instance=RequestContext(request))


def login(request):
    '''
    The view that handles the login request (POST, GET) of a user.
    :param request:
    :return:HttpRedirect object
    '''
    login_form = LoginUserForm(request.POST or None)

    if request.method == "POST" and login_form.is_valid():
        username = login_form.cleaned_data.get('username')
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        return perform_login(request, user)

    args = dict(login_form=login_form)

    return render_to_response("accounts/login.html", args, context_instance=RequestContext(request))


def logout(request):
    '''
    This view handles the logout of the user

    :param request:
    :return: HttpRedirect object to the specified url
    '''
    auth_logout(request)
    return redirect("movie_rama-movies")
