__author__ = 'avlahop'
from django.conf.urls import *
from accounts.views import *


urlpatterns = [
    url(r'^login/$', login, name='accounts-login'),
    url(r'^register/$', register, name='accounts-register'),
    url(r'^logout/$', logout, name='accounts-logout')
]