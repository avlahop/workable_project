from django.contrib.auth.forms import UserCreationForm
from django.forms import CharField, Form, PasswordInput, TextInput
from django.contrib.auth.models import User

__author__ = 'avlahop'


class RegisterUserForm(UserCreationForm):
    first_name = CharField(max_length=30, required=True, widget=TextInput(attrs={"class": "form-control"}))
    last_name = CharField(max_length=30, required=True, widget=TextInput(attrs={"class": "form-control"}))
    password1 = CharField(label=("Password"),
                                widget=PasswordInput(attrs={'class': 'form-control input-sm'}))
    password2 = CharField(label=("Password confirmation"),
                                widget=PasswordInput(attrs={'class': 'form-control input-sm'}),
                                help_text="Enter the same password as above, for verification.")
    class Meta:
        model = User
        fields = ['username', 'password1', 'password2', 'first_name', 'last_name']
        widgets = {
            'username': TextInput(attrs={'class': "form-control"}),
        }

    def save(self, commit=True):
        user = super(RegisterUserForm, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']

        if commit:
            user.save()

        return user


class LoginUserForm(Form):
    username = CharField(widget=TextInput(attrs={'class': 'form-control input-sm', }), max_length=30, required=True)
    password = CharField(max_length=50, required=True, widget=PasswordInput(attrs={'class': 'form-control input-sm', }))
