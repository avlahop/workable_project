## Synopsis

This is the source code of the project "MovieRama" assigned by workable. The main requirements were the following

    1) Users should be able to log into their account or sign up for a new one
    2) Users should be able to add movies by completing a form. MOvies should be persisted and reference the user that submitted them
    3 Users should be able to express their opinion for any movie by either a like or a hate. Users can vote only once for each movie an can change their vote at any time by switching to the opposite vote or by retracting their vote altogether
    4) Users should not be able to vote for the movies they have submitted
    5) Users should be able to view the list of movies and sort them by number of likes, number of hates or date added. 
    6) Users should be able to view all movies submitted by a specific user

## Dependencies
The file requirements.txt contains all the dependencies needed for the project to run. Use pip to install the required dependencies with pip install -U -r requirements.txt. Also python 2.7.x is required to run app.

## Run
To run application just type:

    python manage.py runserver

and visit 127.0.0.1:8000 in your browser

## Tests
To run tests visit directory where manage.py is placed and run

    python manage.py test

To get the coverage of the source code coverage package needs to be installed(included in requirements.txt).
To get coverage report in the directory where manage.py is placed and run

    coverage run --source='.' manage.py test
    coverage report or coverage html to get a more "pretty" report of source code coverage.

## Implementation. 
For the implementation of this project an MVC framework was used. The framework was Django and the programming language was Python. The version of python used was python 2.7.x Also Django's template language was used for some logic in the presentation side (html). For the "styling" part Bootstrap framework was used. Project consists of three apps:

    1) movie_rama
    2) votes
    3) accounts

The first app takes care of all the functionalities regarding showing filtering and adding new movies. Votes has all the views and models necessary to implement the voting system of the project. Accounts app takes care of the user login and register. 

Each app contains the following important modules

    1) models.py: Contains the object used to model the database tables. Each model class represents a database table. Model Book contains some helper methos for incrementing and decrementing likes and hates
    2) forms.py: Contains objects that are used by Django to render the forms in the html templates. 
    3) views.py: One of the most important modules. It contains all the functions/views that implement the logic of the application. They are responsible for creating new objects showing/fetching already created rows from the database, voting and authenticating users.
    4) urls.py: Contain all the url routes. For each route a specific view is called when user visits the specified url. 

For the authentication and registration of new users the built-in system of django was used for speed. 

All requirements were completed.

## Extra
As an extra application uses memcache to cache the movies. Cache get updated when a new movie is added or a movie is voted. Memcache needs to be installed in order for project to work properly. You can comment out the CACHES variable in workable_project/workable_project/settings.py and then django will use the default backend which is Local memory caching.